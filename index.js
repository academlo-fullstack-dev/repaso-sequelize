const express = require("express");
require("dotenv").config();
const { Payments, Memberships, Clients, Clients_Classes, Classes } = require("./models")

const app = express();
const PORT = process.env.PORT || 8000;

app.get("/pagos", async (req, res) => {
    const results = await Payments.findAll({
        include: [Memberships, Clients]
    });

    res.json(results);
});

app.get("/clases/clientes", async (req, res) => {
    const results = await Clients.findAll({
        include: [Classes]
    });

    res.json(results);
});

app.listen(PORT, () => {
    console.log("Servidor escuchando sobre el puerto", PORT);
});