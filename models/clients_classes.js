'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Clients_Classes extends Model {
    static associate(models) {
      this.belongsTo(models.Clients, {
        foreignKey: "idClient"
      });

      this.belongsTo(models.Classes, {
        foreignKey: "idClasses"
      });
    }
  };
  Clients_Classes.init({
    idClasses: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Clients_Classes',
  });
  return Clients_Classes;
};