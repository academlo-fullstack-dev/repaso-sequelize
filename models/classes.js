'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Classes extends Model {
    static associate(models) {
      this.belongsToMany(models.Clients, {
        through: "Clients_Classes",
        foreignKey: "idClasses"
      });

      this.belongsTo(models.Instructors, {
        foreignKey: "instructorId"
      });

      this.belongsToMany(models.Schedules, {
        through: "Classes_Shedules",
        foreignKey: "idSchedules"
      })
    }
  };
  Classes.init({
    name: DataTypes.STRING,
    instructorId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Classes',
  });
  return Classes;
};

//belongsTo -> pertenece a
//hasOne -> tiene una