'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Schedules extends Model {
    static associate(models) {
      this.belongsToMany(models.Classes, {
        through: "Classes_Schedules",
        foreignKey: "idClasses"
      })
    }
  };
  Schedules.init({
    startTime: DataTypes.TIME,
    endTime: DataTypes.TIME
  }, {
    sequelize,
    modelName: 'Schedules',
  });
  return Schedules;
};